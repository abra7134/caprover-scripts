#!/usr/bin/env bash

# Script for updating CapRover by it self through API
MY_DEPENDENCIES=()

# The password to access to CapRover's API
CAPROVER_PASSWORD="${CAPROVER_PASSWORD:-}"
# The URL of CapRover's API
CAPROVER_URL="${CAPROVER_URL:-}"

my_dir="${0%/*}"

set -o errexit

if ! \
  source \
    "${my_dir}"/functions.sh.inc \
  2>/dev/null
then
  echo >&2 "!!! ERROR: Can't load a functions file (functions.sh.inc)"
  echo >&2 "           Please check archive of this script or use 'git checkout --force' command if it cloned from git"
  exit 1
fi

check_dependencies
check_variables

declare -A \
  caprover_answer=()

caprover_request \
  "/api/v2/user/system/versioninfo" \
  "GET"

if [ "${caprover_answer[data.currentVersion]}" = "${caprover_answer[data.latestVersion]}" ]
then
  warning \
    "CapRover is the latest version ${caprover_answer[data.currentVersion]}, update not required"
elif [ ! -v caprover_answer[data.canUpdate] \
       -o "${caprover_answer[data.canUpdate]}" != "true" ]
then
  warning \
    "Cannot upgrade CapRover to version ${caprover_answer[data.latestVersion]}:" \
    "${caprover_answer[description]}"
fi

caprover_changelog="${caprover_answer[data.changeLogMessage]}"

caprover_request \
  "/api/v2/user/system/versioninfo" \
  "POST" \
  "--data-ascii" \
  "{\"latestVersion\": \"${caprover_answer[data.latestVersion]}\"}"

info \
  "${caprover_answer[description]}" \
  "Please wait a minute for caprover to update" \
  "" \
  "${caprover_changelog}"

exit 0
