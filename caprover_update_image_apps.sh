#!/usr/bin/env bash

MY_NAME="Script for updating image-based CapRover's applications through API"
MY_DEPENDENCIES=("sort")

# The password to access to CapRover's API
CAPROVER_PASSWORD="${CAPROVER_PASSWORD:-}"
# The URL of CapRover's API
CAPROVER_URL="${CAPROVER_URL:-}"

my_dir="${0%/*}"

set -o errexit

if ! \
  source \
    "${my_dir}"/functions.sh.inc \
  2>/dev/null
then
  echo >&2 "!!! ERROR: Can't load a functions file (functions.sh.inc)"
  echo >&2 "           Please check archive of this script or use 'git checkout --force' command if it cloned from git"
  exit 1
fi

if [ "${#}" -lt 1 ]
then
  warning \
    "${MY_NAME}" \
    "" \
    "Environment variables:" \
    " * CAPROVER_PASSWORD=\"${CAPROVER_PASSWORD}\"" \
    " * CAPROVER_URL=\"${CAPROVER_URL}\"" \
    "" \
    "Dependencies: ${MY_DEPENDENCIES[*]}" \
    "" \
    "Usage: ${0}                    - Print this help" \
    "       ${0} all                - Try to minor update all image-based applications" \
    "       ${0} <app_name> ...     - Try to minor update only specified image-based applications" \
    "       ${0} -m all             - Try to major update all image-based applications" \
    "       ${0} -m <app_name> ...  - Try to major update only specified image-based applications" \
    ""
fi

check_dependencies
check_variables

declare -A \
  caprover_answer=() \
  registry_answer=()

caprover_request \
  "/api/v2/user/apps/appDefinitions" \
  "GET"

declare -A \
  apps=()

enable_major_upgrade=""

for arg
do
  case "${arg}"
  in
    "-m" )
      enable_major_upgrade="yes"
      ;;
    * )
      apps["${arg}"]="Not found on CapRover"
      ;;
  esac
done

app_id=0
while [ -v caprover_answer[data.appDefinitions.${app_id}.appName] ]
do
  app_name="${caprover_answer[data.appDefinitions.${app_id}.appName]}"

  if [    -v apps["all"] \
       -o -v apps["${app_name}"] ]
  then
    if [ -v caprover_answer[data.appDefinitions.${app_id}.deployedVersion] ]
    then
      app_deployed_version="${caprover_answer[data.appDefinitions.${app_id}.deployedVersion]}"
      if [ -v caprover_answer[data.appDefinitions.${app_id}.versions.${app_deployed_version}.deployedImageName] ]
      then
        app_image="${caprover_answer[data.appDefinitions.${app_id}.versions.${app_deployed_version}.deployedImageName]}"
        if [ "${app_image::12}" != "img-captain-" ]
        then
          apps["${app_name}"]="__${app_image}"
        else
          apps["${app_name}"]="Not a image-based, skipping"
        fi
      else
        apps["${app_name}"]="Not found a 'deployedImageName' parameter, skipping"
      fi
    else
      apps["${app_name}"]="Not found a 'deployedVersion' parameter, skipping"
    fi
  fi

  let app_id+=1
done

unset apps["all"]

for app_name in "${!apps[@]}"
do
  if [ "${apps["${app_name}"]:0:2}" = "__" ]
  then
    app_image_ref="${apps["${app_name}"]:2}"
    app_image_tag="${app_image_ref##*:}"
    app_image_name="${app_image_ref%:*}"

    if [ "${app_image_name#*/}" != "${app_image_name##*/}" ]
    then
      apps["${app_name}"]="Script don't support another from hub.docker.com registries (${app_image_name%%/*}), skipping"
      continue
    fi

    if [[ "${app_image_tag}" =~ ^([^-]+)("-".*)?$ ]]
    then
      app_image_tag_version="${BASH_REMATCH[1]}"
      app_image_tag_suffix="${BASH_REMATCH[2]}"
    else
      apps["${app_name}"]="Unable to parse the image tag (${app_image_tag}), skipping"
      continue
    fi

    IFS="." \
    read \
      -a app_image_tag_version_splitted \
      -r \
    <<<"${app_image_tag_version}"

    if [ "${#app_image_tag_version_splitted[@]}" -gt 3 ]
    then
      apps["${app_name}"]="Script support only semantic versioning with 3 parts, not more (${app_image_tag})"
      continue
    fi

    info \
      "${app_name}: Runned on '${app_image_ref}' image" \
      "${app_name}: Get image versions from registry"

    registry_image_name="${app_image_name}"
    [[ ! "${registry_image_name}" =~ "/" ]] \
    && registry_image_name="library/${app_image_name}"

    registry_request \
      "https://index.docker.io" \
      "https://auth.docker.io" \
      "registry.docker.io" \
      "${registry_image_name}" \
      "/tags/list"

    registry_image_tags=()
    registry_tag_id=0
    while [ -v registry_answer["tags.${registry_tag_id}"] ]
    do
      [ "${registry_answer["tags.${registry_tag_id}"]}" != "latest" ] \
      && registry_image_tags+=("${registry_answer["tags.${registry_tag_id}"]}")

      let registry_tag_id+=1
    done

    # Natural reverse sort of (version) numbers within text
    registry_image_tags_sorted=(
      $(
        printf \
          "%s\n" \
          "${app_image_tag}" \
          "${registry_image_tags[@]}" \
        | \
          sort \
            --reverse \
            --version-sort
      )
    )

    newer_major_tags=()
    newer_minor_tag=""
    for registry_image_tag in "${registry_image_tags_sorted[@]}"
    do
      [ "${registry_image_tag}" = "${app_image_tag}" ] \
      && break

      if [[ "${registry_image_tag}" =~ ^([^-]+)("-".*)?$ ]]
      then
        registry_image_tag_version="${BASH_REMATCH[1]}"
        registry_image_tag_suffix="${BASH_REMATCH[2]}"
      else
        apps["${app_name}"]="Unable to parse the image tag (${registry_image_tag}) from registry, skipping"
        continue 2
      fi

      IFS="." \
      read \
        -a registry_image_tag_version_splitted \
        -r \
      <<<"${registry_image_tag_version}"

      [ "${#app_image_tag_version_splitted[@]}" -ne "${#registry_image_tag_version_splitted[@]}" ] \
      && continue

      [ "${app_image_tag_suffix}" != "${registry_image_tag_suffix}" ] \
      && continue

      case "${#app_image_tag_version_splitted[@]}"
      in
        1 )
          newer_major_tags+=("${registry_image_tag}")
          ;;
        2 )
          if [ "${registry_image_tag_version_splitted[0]}" != "${app_image_tag_version_splitted[0]}" ]
          then
            newer_major_tags+=("${registry_image_tag}")
          elif [ "${registry_image_tag_version_splitted[1]}" != "${app_image_tag_version_splitted[1]}" ]
          then
            newer_minor_tag="${registry_image_tag}"
            break
          fi
          ;;
        3 )
          if [    "${registry_image_tag_version_splitted[0]}" != "${app_image_tag_version_splitted[0]}" \
               -o "${registry_image_tag_version_splitted[1]}" != "${app_image_tag_version_splitted[1]}" ]
          then
            [[    "${#newer_major_tags[@]}" -lt 1
               || "${newer_major_tags[-1]%.*}" != "${registry_image_tag%.*}" ]] \
            && newer_major_tags+=("${registry_image_tag}")
          elif [ "${registry_image_tag_version_splitted[2]}" != "${app_image_tag_version_splitted[2]}" ]
          then
            newer_minor_tag="${registry_image_tag}"
            break
          fi
          ;;
      esac
    done

    if [ -n "${newer_minor_tag}" ]
    then
      info \
        "${app_name}: Found minor upgrade possibility: ${app_image_tag} -> ${newer_minor_tag}"
      app_image_new_tag="${newer_minor_tag}"
    elif [ "${#newer_major_tags[@]}" -gt 0 ]
    then
      info \
        "${app_name}: Found major upgrades possibility: ${app_image_tag} -> ${newer_major_tags[*]}"
      if [ "${enable_major_upgrade}" != "yes" ]
      then
        info \
          "${app_name}: Skipping (to force update use the '-m' option)"
        apps["${app_name}"]="Minor update not required, but major is available (to force update use '-m' option), skipping"
        continue
      else
        app_image_new_tag="${newer_major_tags[-1]}"
      fi
    else
      apps["${app_name}"]="Update not required, skipping"
      continue
    fi

    info "${app_name}: Running update to '${app_image_new_tag}' version"

    caprover_request \
      "/api/v2/user/apps/appData/${app_name}?detached=1" \
      "POST" \
      "--data" \
      "{\"captainDefinitionContent\":\"{\\\"schemaVersion\\\":2,\\\"imageName\\\":\\\"${app_image_name}:${app_image_new_tag}\\\"}\",\"gitHash\":\"\"}"

    caprover_wait_app \
      "update" \
      "${app_image_name}:${app_image_tag} -> ${app_image_name}:${app_image_new_tag}"
  fi
done

show_apps_processing_status

exit 0
