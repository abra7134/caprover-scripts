FROM ubuntu:18.04

RUN \
  export DEBIAN_FRONTEND=noninteractive \
  && apt-get update \
  && apt-get install -y apt-utils \
  && apt-get install -y curl jq \
  && apt-get clean

ADD . /usr/local/bin
