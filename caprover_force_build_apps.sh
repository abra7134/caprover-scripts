#!/usr/bin/env bash

MY_NAME="Script for force (re)building source-based CapRover's applications through API"
MY_DEPENDENCIES=()

# The password to access to CapRover's API
CAPROVER_PASSWORD="${CAPROVER_PASSWORD:-}"
# The URL of CapRover's API
CAPROVER_URL="${CAPROVER_URL:-}"

my_dir="${0%/*}"

set -o errexit

if ! \
  source \
    "${my_dir}"/functions.sh.inc \
  2>/dev/null
then
  echo >&2 "!!! ERROR: Can't load a functions file (functions.sh.inc)"
  echo >&2 "           Please check archive of this script or use 'git checkout --force' command if it cloned from git"
  exit 1
fi

if [ "${#}" -lt 1 ]
then
  warning \
    "${MY_NAME}" \
    "" \
    "Environment variables:" \
    " * CAPROVER_PASSWORD=\"${CAPROVER_PASSWORD}\"" \
    " * CAPROVER_URL=\"${CAPROVER_URL}\"" \
    "" \
    "Dependencies: ${MY_DEPENDENCIES[*]}" \
    "" \
    "Usage: ${0}                    - Print this help" \
    "       ${0} all                - Force (re)build all source-based applications" \
    "       ${0} <app_name> ...     - Force (re)build only specified source-based applications" \
    ""
fi

check_dependencies
check_variables

declare -A \
  caprover_answer=()

caprover_request \
  "/api/v2/user/apps/appDefinitions" \
  "GET"

declare -A \
  apps=()

for arg
do
  apps["${arg}"]="Not found on CapRover"
done

app_id=0
while [ -v caprover_answer[data.appDefinitions.${app_id}.appName] ]
do
  app_name="${caprover_answer[data.appDefinitions.${app_id}.appName]}"

  if [    -v apps["all"] \
       -o -v apps["${app_name}"] ]
  then
    if [ -v caprover_answer[data.appDefinitions.${app_id}.appPushWebhook.pushWebhookToken] ]
    then
      apps["${app_name}"]="__${caprover_answer[data.appDefinitions.${app_id}.appPushWebhook.pushWebhookToken]}"
    else
      apps["${app_name}"]="Not a source-based, skipping"
    fi
  fi

  let app_id+=1
done

unset apps["all"]

for app_name in "${!apps[@]}"
do
  if [ "${apps["${app_name}"]:0:2}" = "__" ]
  then
    info \
      "${app_name}: Run force (re)building the application"

    caprover_request \
      "/api/v2/user/apps/webhooks/triggerbuild" \
      "POST" \
      "--get" \
      "--data" \
      "namespace=captain" \
      "--data" \
      "token=${apps["${app_name}"]:2}"

    caprover_wait_app \
      "rebuild"
  fi
done

show_apps_processing_status

exit 0
