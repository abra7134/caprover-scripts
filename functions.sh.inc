#!/usr/bin/env bash

# Required commands to work scripts correctly
MY_DEPENDENCIES+=("curl" "jq")

# Function to print error messages and exit with '1' error-code
#
# Input: ${*} - Error messages
#
function error {
  for error_message
  do
    echo >&2 "!!! ${error_message}"
  done

  exit 1
}

# Function to print info message
#
# Input: ${*} - Info messages
#
function info {
  for info_message
  do
    echo -e "${info_message}"
  done

  return 0
}

# Function to print warning messages and exit with '0' error-code
#
# Input: ${*} - Warning messages
#
function warning {
  info >&2 "${@}"

  exit 0
}

# Function to send request to CapRover's URI and parse answer to associative array
#
# Input:  ${1}                  - CapRover's URI
#         ${2}                  - Request method (GET, POST)
#         ${*}                  - Another options for curl
#         ${CAPROVER_URL}       - The GLOBAL variable (see the description above)
# Modify: ${caprover_answer[*]} - Associative array with answer parameters from CapRover
#         ${caprover_jwt}       - The JWT for access to CapRover's API
#
function caprover_request {
  local \
    uri="${1}" \
    request="${2}"
  shift 2

  if [ -z "${caprover_jwt}" \
       -a "${uri}" != "/api/v2/login" ]
  then
    caprover_request \
      "/api/v2/login" \
      "POST" \
      "--data-ascii" \
      "{\"password\": \"${CAPROVER_PASSWORD}\"}"

    caprover_jwt="${caprover_answer[data.token]}"
  fi

  local -A \
    json_answer=()

  json_request \
    "${CAPROVER_URL%/}/${uri#/}" \
    "${request}" \
    --header "x-captain-auth: ${caprover_jwt}" \
    --header "x-namespace: captain" \
    "${@}"

  local \
    key="" \
    value=""

  # Copy ${json_answer[@]} to ${caprover_answer[@]}
  caprover_answer=()
  for key in "${!json_answer[@]}"
  do
    caprover_answer[${key}]="${json_answer[${key}]}"
  done

  if [    "${caprover_answer[status]}" != "100" \
       -a "${caprover_answer[status]}" != "101" ]
  then
    error \
      "Unable to ${request} ${uri} request: ${caprover_answer[description]} (${caprover_answer[status]})"
  fi

  return 0
}

# Fucntion to wait while an application is rebuild/update
#
# Input:  ${1}         - Name of operation (needed only for info messages)
#         ${2}         - Additional info messages (if operation is successful)
#         ${app_name}  - Application name
# Modify: ${apps[@]}   - The array with all applications
#
function caprover_wait_app {
  local \
    operation="${1}" \
    info="${2:-}" \
    attempts=100

  while
    caprover_request \
      "/api/v2/user/apps/appData/${app_name}" \
      "GET";
    [    "${caprover_answer[data.isAppBuilding]}" = "true" \
      -a "${attempts}" -gt 0 ]
  do
    attempts=$((attempts-1))
    info \
      "${app_name}: Waiting for application ${operation%e}ing to complete (remained ${attempts} attempts)"
    sleep 5
  done

  operation="${operation^}"
  if [ "${attempts}" -lt 1 ]
  then
    apps["${app_name}"]="${operation} wait timeout"
  elif [ "${caprover_answer[data.isBuildFailed]}" = "true" ]
  then
    apps["${app_name}"]="${operation} failed (for details please see build logs at web-console)"
  else
    apps["${app_name}"]="${operation%e}ed${info:+ (${info})}"
  fi

  return 0
}

# Function to send request to specified URL and parse JSON-answer to associative array
#
# Input:  ${1}              - URL to request
#         ${2}              - Request method (GET, POST)
#         ${*}              - Another options for curl
# Modify: ${json_answer[*]} - Associative array with answer parameters
#
function json_request {
  local \
    url="${1}" \
    request="${2}" \
    answer_json="" \
    answer_tsv=""
  shift 2

  answer_json=$(
    curl \
      "${@}" \
      --connect-timeout 10 \
      --header "Content-Type: application/json;charset=utf-8" \
      --request "${request}" \
      --show-error \
      --silent \
      "${url}"
  )

  answer_tsv=$(
    # Don't use the paths(scalars) function instead of path(..|) in jq,
    # because there is bug of processing 'false' values in json
    # See for details https://github.com/stedolan/jq/issues/1163
    jq --raw-output '
      path(.. | select(type != "array" and type != "object")) as $path
      | [
          ($path | map(tostring) | join(".")),
          getpath($path)
        ]
      | @tsv
    ' \
    <<<"${answer_json}"
  )

  local \
    key="" \
    value=""

  json_answer=()
  while
    IFS=$'\t' read -r key value
  do
    json_answer[${key}]="${value}"
  done \
  <<< "${answer_tsv}"

  return 0
}

# Function to send request to registry (for example: hub.docker.com)
#
#  Input: ${1}                  - The URL to registry index
#         ${2}                  - The URL to registry authentication service
#         ${3}                  - The name of registry service (required for authorization)
#         ${4}                  - The image name for which request to be send
#         ${5}                  - The URI to registry index
# Modify: ${registry_answer[*]} - Associative array with answer parameters
#
function registry_request {
  local \
    registry_url="${1}" \
    registry_auth_url="${2}" \
    registry_service="${3}" \
    image_name="${4}" \
    uri="${5}"

  local -A \
    json_answer=() \
    registry_jwt=""

  json_request \
    "${registry_auth_url}/token?service=${registry_service}&scope=repository:${image_name}:pull" \
    "GET"

  registry_jwt="${json_answer[token]}"

  json_request \
    "${registry_url%/}/v2/${image_name}/${uri#/}" \
    "GET" \
    --header "Authorization: Bearer ${registry_jwt}"

  registry_answer=()
  for key in "${!json_answer[@]}"
  do
    registry_answer[${key}]="${json_answer[${key}]}"
  done

  return 0
}

# Function to print the applications processing status
#
# Input: ${apps[@]} - Array with all processed applications and their statuses
#
function show_apps_processing_status {
  local \
    app_name=""

  if [ "${#apps[@]}" -gt 0 ]
  then
    info \
      "" \
      "Applications processing status:" \
      ""

    for app_name in "${!apps[@]}"
    do
      printf -- \
        "* %-20s - %s\n" \
        "${app_name}" \
        "${apps["${app_name}"]}"
    done
  else
    info \
      "Nothing to process, exiting..."
  fi
}

# Function to check global variables
#
# Input: ${CAPROVER_URL}      - The GLOBAL variable (see the description above)
#        ${CAPROVER_PASSWORD} - The GLOBAL variable (see the description above)
#
function check_variables {
  if [ -z "${CAPROVER_URL}" ]
  then
    error \
      "The environment variable \${CAPROVER_URL} must be non-empty value"
  elif [[ ! "${CAPROVER_URL}" =~ ^https?://[[:alnum:]\.:/_-]+$ ]]
  then
    error \
      "The environment variable \${CAPROVER_URL} must be a correct HTTP URL (https://example.com)" \
      "not \"${CAPROVER_URL}\""
  elif [ -z "${CAPROVER_PASSWORD}" ]
  then
    error \
      "The environment variable \${CAPROVER_PASSWORD} must be non-empty value"
  fi

  return 0
}

# Function to check dependencies
#
# Input: ${MY_DEPENDENCIES[@]} - THe GLOBAL variable (see the description above)
#
function check_dependencies {
  for required_command in "${MY_DEPENDENCIES[@]}"
  do
    if ! \
      type \
        -P "${required_command}" \
      >/dev/null
    then
      error \
        "The required command '${required_command}' is not exist" \
        "Please check your PATH environment variable" \
        "And install a required command through your package manager"
    fi
  done

  return 0
}
