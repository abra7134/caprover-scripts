# Scripts for CapRover

* [caprover_force_build_apps.sh](#caprover_force_build_appssh)
* [caprover_update.sh](#caprover_updatesh)
* [caprover_update_image_apps.sh](#caprover_update_image_appssh)

## Requirements

Ubuntu not lower than **18.04** is required for correct operation of scripts.

## caprover_force_build_apps.sh

Script for force (re)building source-based CapRover's applications through API

### Configuration

The following environment variables must be set for proper operation:
* `CAPROVER_PASSWORD` - The password for access to CapRover
* `CAPROVER_URL` - The CapRover's URL

### Running

As parameters, the script takes the name of the applications that you want to force (re)building.
The <all> keyword will prompt the script to update all source-based applications.

Examples (methods for sending environment variables to the script were described in `caprover_update.sh` script):

To help view:
```sh
$ ./caprover_force_build_apps.sh
```

To force (re)build `test1` and `test2` applications:
```sh
$ ./caprover_force_build_apps.sh test1 test2
```

To force (re)build all applications on CapRover instance:
```sh
$ ./caprover_force_build_apps.sh all
```

When finished, the script displays the processing status of all applications:
```sh

Run force (re)building the 'test1' application

Applications processing status:

* test3                - Not found on CapRover
* test2                - Not a source-based, skipping
* test1                - Build webhook has triggered

```

## caprover_update.sh

Script for updating CapRover by it self through API.

### Configuration

The following environment variables must be set for proper operation:
* `CAPROVER_PASSWORD` - The password for access to CapRover
* `CAPROVER_URL` - The CapRover's URL

### Running

You can set environment variables just before running the script:

```sh
$ CAPROVER_PASSWORD="password" CAPROVER_URL="https://localhost:3000" ./caprover_update.sh
```

Or through the export of variables:

```sh
$ export CAPROVER_PASSWORD="password"
$ export CAPROVER_URL="https://localhost:3000"
$ ./caprover_update.sh
```

Or you can set variables in the file:

```sh
$ cat >.env <<EOF
CAPROVER_PASSWORD="password"
CAPROVER_URL="https://localhost:3000"
EOF
$ source .env && ./caprover_update.sh
```

## caprover_update_image_apps.sh

Script for updating image-based CapRover's applications through API

### Configuration

The following environment variables must be set for proper operation:
* `CAPROVER_PASSWORD` - The password for access to CapRover
* `CAPROVER_URL` - The CapRover's URL

### Running

As parameters, the script takes the name of the applications that you want to try to update.
The <all> keyword will prompt the script to update all image-based applications.

By default, the script tries to update to the nearest minor version,
but if you need to update to the nearest major version, then you must specify the `-m` option.

The script supports updating versions in semantic form (semver) and its derivatives, for example:
* v1
* 1.2
* 1.4.5
* 1.6.7-alpine

Examples (methods for sending environment variables to the script were described in `caprover_update.sh` script):

To help view:
```sh
$ ./caprover_update_image_apps.sh
```

To try minor update `test1` and `test2` applications:
```sh
$ ./caprover_update_image_apps.sh test1 test2
```

Try to major update `test3` and `test4` applications:
```sh
$ ./caprover_update_image_apps.sh -m test3 test4
```

Try to minor update all applications on CapRover instance:
```sh
$ ./caprover_update_image_apps.sh all
```

When finished, the script displays the processing status of all applications:
```sh
Applications processing status:

* test1                - Updated (redmine:4.1.0 -> redmine:4.1.7)
* test2                - Update not required, skipping
* test3                - Minor update not required, but major is available (to force update use '-m' option), skipping
* test4                - Update failed (for details please see build logs at web-console)
```
